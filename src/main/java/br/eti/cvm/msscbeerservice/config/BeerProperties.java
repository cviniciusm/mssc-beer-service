package br.eti.cvm.msscbeerservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "sfg.brewery", ignoreUnknownFields = false)
//@Component
public class BeerProperties {

	private String beerInventoryServiceHost;

}
