package br.eti.cvm.msscbeerservice.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.eti.cvm.msscbeerservice.services.BeerService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api/v1/beerUpc")
@RestController
public class BeerUpcController {

	private static final Integer DEFAULT_PAGE_NUMBER = 0;

	private static final Integer DEFAULT_PAGE_SIZE = 25;

	private final BeerService beerService;

	@GetMapping("/{upc}")
	public ResponseEntity<?> getBeerByUpc(@PathVariable("upc") String upc) {

		return new ResponseEntity<>(beerService.getByUpc(upc), HttpStatus.OK);
	}

}