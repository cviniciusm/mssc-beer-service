package br.eti.cvm.msscbeerservice.services;

import java.util.UUID;

import org.springframework.data.domain.PageRequest;

import br.eti.cvm.msscbeerservice.web.model.BeerDto;
import br.eti.cvm.msscbeerservice.web.model.BeerPagedList;
import br.eti.cvm.msscbeerservice.web.model.BeerStyleEnum;

public interface BeerService {

	BeerDto getById(UUID beerId, Boolean showInventoryOnHand);

	BeerDto saveNewBeer(BeerDto beerDto);

	BeerDto updateBeer(UUID beerId, BeerDto beerDto);

	BeerPagedList listBeers(String beerName, BeerStyleEnum beerStyle, PageRequest pageRequest,
			Boolean showInventoryOnHand);

	BeerDto getByUpc(String upc);

}
