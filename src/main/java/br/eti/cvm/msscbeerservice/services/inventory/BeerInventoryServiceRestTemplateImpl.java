package br.eti.cvm.msscbeerservice.services.inventory;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.eti.cvm.msscbeerservice.config.BeerProperties;
import br.eti.cvm.msscbeerservice.services.inventory.model.BeerInventoryDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BeerInventoryServiceRestTemplateImpl implements BeerInventoryService {

	private final String INVENTORY_PATH = "/api/v1/beer/{beerId}/inventory";

	private final RestTemplate restTemplate;

	private final BeerProperties beerProperties;

	private String beerInventoryServiceHost;

	public BeerInventoryServiceRestTemplateImpl(BeerProperties beerProperties,
			RestTemplateBuilder restTemplateBuilder) {
		this.beerProperties = beerProperties;
		this.restTemplate = restTemplateBuilder.build();
	}

	@PostConstruct
	private void init() {
		this.beerInventoryServiceHost = beerProperties.getBeerInventoryServiceHost();
	}

	@Override
	public Integer getOnhandInventory(UUID beerId) {

		log.debug("Calling Inventory Service");

		ResponseEntity<List<BeerInventoryDto>> responseEntity = restTemplate.exchange(
				beerInventoryServiceHost + INVENTORY_PATH, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BeerInventoryDto>>() {
				}, (Object) beerId);

		// sum from inventory list
		Integer onHand = Objects.requireNonNull(responseEntity.getBody()).stream()
				.mapToInt(BeerInventoryDto::getQuantityOnHand).sum();

		return onHand;
	}
}