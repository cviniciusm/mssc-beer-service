package guru.sfg.common.events;

import br.eti.cvm.msscbeerservice.web.model.BeerDto;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BrewBeerEvent extends BeerEvent {

	private static final long serialVersionUID = -5131955537231720422L;

	public BrewBeerEvent(BeerDto beerDto) {
		super(beerDto);
	}
}