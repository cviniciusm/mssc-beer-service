package br.eti.cvm.msscbeerservice.web.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.eti.cvm.msscbeerservice.bootstrap.BeerLoader;
import br.eti.cvm.msscbeerservice.services.BeerService;
import br.eti.cvm.msscbeerservice.web.model.BeerDto;
import br.eti.cvm.msscbeerservice.web.model.BeerStyleEnum;

@WebMvcTest(BeerUpcController.class)
class BeerUpcControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;

	@MockBean
	BeerService beerService;

	@Test
	void testGetBeerByUpc() throws Exception {
		given(beerService.getByUpc(any())).willReturn(getValidBeerDto());

		mockMvc.perform(get("/api/v1/beerUpc/" + getRandomNumericString(13)).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	BeerDto getValidBeerDto() {
		return BeerDto.builder()
				.beerName("My Beer")
				.beerStyle(BeerStyleEnum.ALE)
				.price(new BigDecimal("2.99"))
				.upc(BeerLoader.BEER_1_UPC).build();
	}

	String getRandomNumericString(int n) {

		// chose a Character random from this String
		String numericString = "0123456789";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to numericString variable length
			int index = (int) (numericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(numericString.charAt(index));
		}

		return sb.toString();
	}

}
